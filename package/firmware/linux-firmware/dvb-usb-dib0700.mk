Package/dvb-usb-dib0700-firmware = $(call Package/firmware-default,dib0700 firmware)
define Package/dvb-usb-dib0700.mk/install
	$(INSTALL_DIR) $(1)/lib/firmware
	$(INSTALL_DATA) \
		$(PKG_BUILD_DIR)/dvb-usb-dib0700-1.20.fw $(1)/lib/firmware
endef
$(eval $(call BuildPackage,dvb-usb-dib0700-firmware))
